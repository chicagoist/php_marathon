<?php
if ($_GET['id']) {
    require_once 'db_accesses.php';
    
    $stmt = $pdo->prepare('DELETE FROM article WHERE id = :id');
    $stmt->bindValue(':id', $_GET['id']);
    $stmt->execute();
}
header('Location: index.php');
